WITH sales_data AS (
    SELECT
        t.time_id,
        t.day_number_in_week,
        t.calendar_week_number,
        t.day_name,
        SUM(amount_sold) AS sales_amount
    FROM
        sh.sales
        INNER JOIN sh.times t ON sh.sales.time_id = t.time_id
    WHERE
        t.calendar_week_number BETWEEN 49 AND 51
        AND YEAR(t.time_id) = 1999
    GROUP BY
        t.time_id,
        t.calendar_week_number,
        t.day_name,
        t.day_number_in_week
)

SELECT
    time_id,
    day_name,
    calendar_week_number,
    sales_amount,
    SUM(sales_amount) OVER (PARTITION BY calendar_week_number ORDER BY time_id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cum_sum,
    CASE
        WHEN day_number_in_week = 1 THEN
            ROUND(AVG(sales_amount) OVER (ORDER BY time_id ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING), 2)
        WHEN day_number_in_week = 5 THEN
            ROUND(AVG(sales_amount) OVER (ORDER BY time_id ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING), 2)
        ELSE
            ROUND(AVG(sales_amount) OVER (ORDER BY time_id ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING), 2)
    END AS centered_3_day_avg
FROM sales_data
ORDER BY time_id;


-- Here is the Explanation of what I did in the code:

-- 1. WITH clause (Creating the `sales_data` temporary table):
--    This part of the query is creating a temporary table named `sales_data`. 
--    It's selecting sales data from the `sh.sales` and `sh.times` tables for the 49th, 50th, and 51st weeks of 1999.
--     The selected columns are `time_id`, `day_number_in_week`, `calendar_week_number`, `day_name`, and the total sales amount (`sales_amount`) for each day.

-- 2. SELECT statement (Querying from the `sales_data` temporary table):
--    This part of the query is doing several things:
--    - Selecting Columns: It's selecting the `time_id`, `day_name`, `calendar_week_number`, and `sales_amount` columns from the `sales_data` table.
--    - Calculating Cumulative Sum: The `SUM(sales_amount) OVER (PARTITION BY calendar_week_number ORDER BY time_id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cum_sum` part is calculating a cumulative sum of sales for each week. 
--    It's summing up all the sales from the start of the week up to the current row.
--    - Calculating Centered 3-Day Average:** The `CASE` statement is calculating a centered 3-day average of sales (`centered_3_day_avg`). 
--    The average is calculated differently depending on the day of the week:
--      - For Monday (`day_number_in_week = 1`), it's calculating the average sales of Saturday, Sunday, Monday, and Tuesday.
--      - For Friday (`day_number_in_week = 5`), it's calculating the average sales of Thursday, Friday, Saturday, and Sunday.
--      - For other days, it's calculating the average sales of the previous day, the current day, and the next day.
--    - Ordering the Result:
--    Finally, the `ORDER BY time_id` clause is ordering the result by `time_id`.

